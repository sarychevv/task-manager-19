package ru.t1.sarychevv.tm.command.user;

import ru.t1.sarychevv.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

    @Override
    public String getName() {
        return "change-user-password";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }
}
