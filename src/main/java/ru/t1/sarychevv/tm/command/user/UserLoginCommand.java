package ru.t1.sarychevv.tm.command.user;

import ru.t1.sarychevv.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "User login";
    }

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
