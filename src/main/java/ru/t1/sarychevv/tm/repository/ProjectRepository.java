package ru.t1.sarychevv.tm.repository;

import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public void create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
    }

    public void create(final String name) {
        final Project project = new Project();
        project.setName(name);
        add(project);
    }

}
