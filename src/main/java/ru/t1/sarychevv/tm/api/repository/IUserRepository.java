package ru.t1.sarychevv.tm.api.repository;

import ru.t1.sarychevv.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void add(User user);

    List<User> findAll();

    User findOneById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
