package ru.t1.sarychevv.tm.command.system;

import ru.t1.sarychevv.tm.api.service.ICommandService;
import ru.t1.sarychevv.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
