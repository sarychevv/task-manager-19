package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.api.repository.IRepository;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    M updateById(String id, String name, String description);

    M updateByIndex(Integer index, String name, String description);

    M changeStatusById(String id, Status status);

    M changeStatusByIndex(Integer index, Status status);
}
