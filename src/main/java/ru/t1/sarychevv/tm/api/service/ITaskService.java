package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAllByProjectId(String projectId);

    void create(String name, String description);
}
