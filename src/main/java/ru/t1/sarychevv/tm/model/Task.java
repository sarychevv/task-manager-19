package ru.t1.sarychevv.tm.model;

import ru.t1.sarychevv.tm.api.model.IWBS;

public final class Task extends AbstractModel implements IWBS {

    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
