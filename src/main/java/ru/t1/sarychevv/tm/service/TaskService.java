package ru.t1.sarychevv.tm.service;

import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.api.service.ITaskService;
import ru.t1.sarychevv.tm.exception.field.DescriptionEmptyException;
import ru.t1.sarychevv.tm.exception.field.NameEmptyException;
import ru.t1.sarychevv.tm.model.Task;

import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
    }

    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(projectId);
    }
}
