package ru.t1.sarychevv.tm.api.repository;

import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(M model);

    //void create(String name, String description);

    //void create(String name);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void clear();

    Integer getSize();

    Boolean existsById(String id);
}
