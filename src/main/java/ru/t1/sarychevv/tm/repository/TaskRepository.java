package ru.t1.sarychevv.tm.repository;

import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public void create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
    }

    public void create(final String name) {
        final Task task = new Task();
        task.setName(name);
        add(task);
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}
